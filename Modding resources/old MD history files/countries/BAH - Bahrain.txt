﻿capital = 856

oob = "BAH_2000"

set_convoys = 40
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	islamic_sectarian_conflicts
	arab_league_member
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = {
			popularity = 20
		}
		nationalist = {
			popularity = 4
		}
		monarchist = {
			popularity = 40
		}
		reactionary = {
			popularity = 3
		}
		conservative = {
			popularity = 16
		}
		social_liberal = {
			popularity = 5
		}
		social_democrat = {
			popularity = 7
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = monarchist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "King Hamad"
	picture = "Hamad.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Abdulrahman al-Nuaimi"
	picture = "Abdulrahman_al-Nuaimi.dds"
	ideology = autocrat
}
create_country_leader = {
	name = "Saeed al-Shehabi"
	picture = "Saeed_al-Shehabi.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Ghanim Al-Buaneen"
	picture = "Ghanim_Buaneen.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Jassim Abdula'al"
	picture = "Jassim_Abdul_Aal.dds"
	ideology = moderate
}
create_country_leader = {
	name = "Ali Salman"
	picture = "Ali_Salman.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Matar Matar"
	picture = "Matar.dds"
	ideology = islamic_republican
}
create_country_leader = {
	name = "Ahmad Juma'a"
	picture = "Ahmad_Jumaa.dds"
	ideology = libertarian
}
create_country_leader = {
	name = "Anonymous activists"
	picture = "Anonymous_activists.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Hassan Tariq Al-Hassan"
	picture = "Hassan_Traiq.dds"
	ideology = national_socialist
}
create_country_leader = {
	name = "Abdullah Hashem"
	picture = "Abdullah_G_Hashem.dds"
	ideology = green
}
create_country_leader = {
	name = "Ahmad Al-Thawadi"
	picture = "Ahmad_Al-Thawadi.dds"
	ideology = marxist
}

create_field_marshal = { 
	name = "Khalifa bin Ahmed" 
	picture = "Khalifa_bin_Ahmed.dds"
	traits = { offensive_doctrine } 
	skill = 4
}

create_corps_commander = { 
	name = "Daij bin Salman" 
	picture = "Duji_bin_Salman.dds"
	traits = { commando } 
	skill = 2
}

create_corps_commander = { 
	name = "Theyab bin Saqer Al Noaimi" 
	picture = "Theyab_biin_Noaimi.dds"
	traits = { panzer_leader } 
	skill = 3
}

create_navy_leader = {
	name = "Yusuf al-Maluallah"
	picture = "Yusuf_Maluallab.dds"
	traits = { blockade_runner }
	skill = 4
}