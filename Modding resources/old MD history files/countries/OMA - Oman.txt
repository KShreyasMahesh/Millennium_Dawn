﻿capital = 294

oob = "OMA_2000"

set_convoys = 220
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = ibadi_islam

add_ideas = {
	population_growth_explosion
	arab_league_member
	extensive_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 10
		}
		nationalist = {
			popularity = 5
		}
		monarchist = {
			popularity = 70
		}
		social_democrat = {
			popularity = 15
		}

	}
	
	ruling_party = monarchist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Qabus ibn Said"
	picture = "Qabus.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Musa'ab Hassan"
	picture = "Musaab_Hassan.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Khaled Al-Mawali"
	picture = "Khaled_Mawali.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Ahmed Al-Khalili"
	picture = "Khalili.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Salim bin Musallam"
	picture = "Salim_Musallam.dds"
	ideology = autocrat
}

create_field_marshal = {
	name = "Qabus ibn Said"
	picture = "generals/Qabus_ibn_Said.dds"
	traits = { old_guard thorough_planner }
	skill = 2
}

create_corps_commander = { 
	name = "Ahmed bin Harith al-Nabhani" 
	picture = "generals/Ahmed_al-Nabhani.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = { 
	name = "Salim bin Musallam" 
	picture = "generals/Salim_bin_Musallam.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = { 
	name = "Mohammed bin Nasser Al-Rasabi"
	picture = "generals/Mohammed_N_Al_Rasabi.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = { 
	name = "Khaalfan el-Zaadgali" 
	picture = "generals/Khalfan_al-Zaadgali.dds"
	traits = { urban_assault_specialist panzer_leader }
	skill = 1
}

create_navy_leader  = { 
	name = "Qabus ibn Said"
	picture = "admirals/Qabus_ibn_Said.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
} 

create_navy_leader  = { 
	name = "Khalifa bin Hamad Al Qasimi"
	picture = "admirals/Khalifa_Al_Qasimi.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader  = { 
	name = "Al-Sayyed Shihab" 
	picture = "admirals/Al-Sayyed_Shihab.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader  = { 
	name = "Hassan bin Ali al-Sayabi" 
	picture = "admirals/Hassan_Ali_al-Sayabi.dds"
	traits = { spotter }
	skill = 1
} 

create_navy_leader  = { 
	name = "Khamis bin Salem Al-Jabri"
	picture = "admirals/Khamis_S_Al_Jabri.dds"
	traits = { old_guard_navy air_controller }
	skill = 2
}

create_navy_leader  = { 
	name = "Hamid bin Saif Al-Sheidi"
	picture = "admirals/Hamid_Al_Sheidi.dds"
	traits = { ironside }
	skill = 1
}

create_navy_leader  = { 
	name = "Abdullah bin Khamis Al-Raisi" 
	picture = "admirals/Abdullaah_al-Raisi.dds"
	traits = { fly_swatter }
	skill = 1
} 
