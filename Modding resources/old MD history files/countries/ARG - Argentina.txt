﻿capital = 847

oob = "ARG_2000"

set_convoys = 80
set_stability = 0.5

add_namespace = {
	name = "arg_unit_leader"
	type = unit_leader
}

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {
	parties = {
		islamist = { popularity = 0.1 }
		nationalist = { popularity = 1 }
		reactionary = { popularity = 2 }
		conservative = { popularity = 30.9 }
		market_liberal = { popularity = 6 }
		social_liberal = { popularity = 25 }
		social_democrat = { popularity = 5 }
		progressive = { popularity = 10 }
		democratic_socialist = { popularity = 16 }
		communist = { popularity = 4 }
	}
	ruling_party = social_liberal
	last_election = "1999.12.10"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = ENG
	modifier = falkland_conflict_argentina
}

create_country_leader = {
	name = "Alejandro Biondini"
	picture = "Alejandro_Biondini.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Mauricio Macri"
	picture = "Mauricio_Macri.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Fernando de la Rua"
	picture = "Fernando_de_la_Rua.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Margarita Stolbizer"
	picture = "Margarita_Stolbizer.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Antonio Bonfatti"
	picture = "Antonio_Bonfatti.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Jorge Altamira"
	picture = "Jorge_Altamira.dds"
	ideology = marxist
}

create_field_marshal = {
	name = "Diego Luis Suñer"
	picture = "generals/Diego_Suner.dds"
	traits = { }
	skill = 2
}

create_field_marshal = {
	name = "Bari del Valle Sosa"
	picture = "generals/Bari_del_Valle_Sosa.dds"
	traits = { fast_planner }
	skill = 1
}

create_corps_commander = {
	name = "Enrique Victor Amrein"
	picture = "generals/Enrique_Victor_Amrein.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Santiago Ferreyra"
	picture = "generals/Santiago_Ferreyra.dds"
	skill = 2
}

create_corps_commander = {
	name = "Joaquín Estrada"
	picture = "generals/Joaquin_Estrada.dds"
	skill = 1
}

create_corps_commander = {
	name = "Agustin Cejas"
	picture = "generals/Agustin_Cejas.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alberto Corvalan"
	picture = "generals/Alberto_Corvalan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alberto Sigon"
	picture = "generals/Alberto_Sigon.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Aldo Fernandez"
	picture = "generals/Aldo_Fernandez.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Aldo Sala"
	picture = "generals/Aldo_Sala.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alejandro Martelletti"
	picture = "generals/Alejandro_Martelletti.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Alfredo Pérez Aquino"
	picture = "generals/Carlos_Aquino.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Nogueira"
	picture = "generals/Carlos_Nogueira.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Podio"
	picture = "generals/Carlos_Podio.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Sityar"
	picture = "generals/Carlos_Sityar.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Claudio Gallardo"
	picture = "generals/Claudio_Gallardo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Clemente Magallanes"
	picture = "generals/Clemente_Magallanes.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Eduardo Varela"
	picture = "generals/Daniel_Varela.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Claudio Ernesto Pasqualini"
	picture = "generals/Ernesto_Pasqualini.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Federico Sidders"
	picture = "generals/Federico_Sidders.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Fernando Mauricio Ros"
	picture = "generals/Fernando_Ros.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Gerardo Ferrara"
	picture = "generals/Gerardo_Ferrara.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "German Monge"
	picture = "generals/German_Monge.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Guillermo Pereda"
	picture = "generals/Guillermo_Pereda.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Guillermo Ángel Tabernero"
	picture = "generals/Guillermo_Tabernero.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Booth"
	picture = "generals/Gustavo_Booth.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Luzuriaga"
	picture = "generals/Gustavo_Luzuriaga.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Planes"
	picture = "generals/Gustavo_Planes.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Vidal"
	picture = "generals/Gustavo_Vidal.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Héctor Horacio Prechi"
	picture = "generals/Hector_Prechi.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hugo Gargano"
	picture = "generals/Hugo_Gargano.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Hugo Leonard"
	picture = "generals/Hugo_Leonard.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Javier Abregu"
	picture = "generals/Javier_Abregu.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Javier Aquino"
	picture = "generals/Javier_Aquino.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jose Navarro"
	picture = "generals/Jose_Navarro.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "José Antonio Saumell Robert"
	picture = "generals/Jose_Robert.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Jose Stanchina"
	picture = "generals/Jose_Stanchina.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "José Luis Yofre"
	picture = "generals/Jose_Yofre.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Juan Antonio Zamora"
	picture = "generals/Juan_Antonio_Zamora.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Adrián Campitelli"
	picture = "generals/Juan_Campitelli.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Juan Gettig"
	picture = "generals/Juan_Gettig.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Paleo"
	picture = "generals/Juan_Paleo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Pulleiro"
	picture = "generals/Juan_Pulleiro.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Justo Treviranus"
	picture = "generals/Justo_Treviranus.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Luis Ricciardi"
	picture = "generals/Luis_Ricciardi.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Maria Pansa"
	picture = "generals/Maria_Pansa.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Martin Deimundo"
	picture = "generals/Martin_Deimundo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Miguel Lugand"
	picture = "generals/Miguel_Lugand.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Norberto Pastor"
	picture = "generals/Norberto_Pastor.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Osvaldo Guardone"
	picture = "generals/Osvaldo_Guardone.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Osvaldo Jose Suárez"
	picture = "generals/Osvaldo_Suarez.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Sergio Santulario"
	picture = "generals/Sergio_Santulario.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Tomas Moyano"
	picture = "generals/Tomas_Moyano.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Bernardo Noziglia"
	picture = "admirals/Bernardo_Noziglia.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Cesar Recalde"
	picture = "admirals/Cesar_Recalde.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Dardo Difalco"
	picture = "admirals/Dardo_Difalco.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "David Burden"
	picture = "admirals/David_Burden.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Bacchi"
	picture = "admirals/Eduardo_Bacchi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Malchiodi"
	picture = "admirals/Eduardo_Malchiodi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Sancet"
	picture = "admirals/Eduardo_Sancet.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Francisco Medrano"
	picture = "admirals/Francisco_Medrano.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gabriel Gonzalez"
	picture = "admirals/Gabriel_Gonzalez.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "German Arbizu"
	picture = "admirals/German_Arbizu.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Guillermo Cormick"
	picture = "admirals/Guillermo_Cormick.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gustavo Iglesias"
	picture = "admirals/Gustavo_Iglesias.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gustavo Vignale"
	picture = "admirals/Gustavo_Vignale.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Jorge Cisneros"
	picture = "admirals/Jorge_Cisneros.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Jose Villan"
	picture = "admirals/Jose_Villan.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Luis Gottardo"
	picture = "admirals/Luis_Gottardo.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Luis Mazzeo"
	picture = "admirals/Luis_Mazzeo.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Marcelo Eduardo Hipolito Srur"
	picture = "admirals/Marcelo_Srur.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Marcos Henson"
	picture = "admirals/Marcos_Henson.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Maria Uriarte"
	picture = "admirals/Maria_Uriarte.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Miguel Ángel Máscolo"
	picture = "admirals/Miguel_Mascolo.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Oscar Pichel"
	picture = "admirals/Oscar_Pichel.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Oscar Vivas"
	picture = "admirals/Oscar_Vivas.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Osvaldo Vernazza"
	picture = "admirals/Osvaldo_Vernazza.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Pedro Galardi"
	picture = "admirals/Pedro_Galardi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Rafael Prieto"
	picture = "admirals/Rafael_Prieto.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Rodolfo Larrosa"
	picture = "admirals/Rodolfo_Larrosa.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Zenon Bolino"
	picture = "admirals/Zenon_Bolino.dds"
	traits = {  }
	skill = 1
}

2016.1.1 = {
	set_politics = {
		ruling_party = conservative
		last_election = "2015.12.10"
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Elisa Carrió"
		picture = "Elisa_Carrio.dds"
		ideology = liberalist
	}
}
2017.1.1 = {

oob = "ARG_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#FARA 83
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	#CITER 155mm L33
	gw_artillery = 1
	Arty_upgrade_1 = 1
	
	#Cañón 155mm L45 CALA 30
	artillery_1 = 1
	
	#Argentinian mortars
	
	
	#TAM
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	
	#VCTM
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	
	#Pampero
	SP_R_arty_equipment_0 = 1
	SP_R_arty_equipment_1 = 1
	
	#Patagon
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	
	#VLEGA Gaucho
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	
	#Lipán M3
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	
	#AMC VCI
	Early_APC = 1
	APC_1 = 1
	
	#VCTP
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	
	#A-4AR Fightinhawk
	early_fighter = 1
	Strike_fighter1 = 1
	Strike_fighter2 = 1
	
	#IA 58 Pucara
	L_Strike_fighter1 = 1
	L_Strike_fighter2 = 1
	
	#Azopardo-Class
	frigate_1 = 1
	
	#Espora Class
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	
	#Malvinas
	missile_corvette_2 = 1
	missile_corvette_3 = 1
	
	#For templates
	combat_eng_equipment = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	
	ENG_MBT_1 = 1
	
	landing_craft = 1
	

}

add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_6
	rio_pact_member
	    stable_growth
		defence_01
	edu_04
	health_03
	social_04
	bureau_03
	police_05
	volunteer_army
	volunteer_women
	Major_Non_NATO_Ally
	international_bankers
	small_medium_business_owners
	landowners
	civil_law
	cartels_2
}
set_country_flag = gdp_6
set_country_flag = Major_Non_NATO_Ally
set_country_flag = positive_international_bankers
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 84
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 1
		}
		
		neutrality = { 
			popularity = 15
		}
	}
	
	ruling_party = democratic
	last_election = "2015.12.10"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Cristina Fernández de Kirchner"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "ARG_Cristina_Fernandez_de_Kirchner.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Mauricio Macri"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "ARG_Mauricio_Macri.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Nicolás del Caño"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "ARG_Nicolas_del_Cano.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sergio Massa"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "ARG_Sergio_Massa.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

create_country_leader = {
	name = "Alejandro Biondini"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "ARG_Alejandro_Biondini.dds"
	expire = "2065.1.1"
	ideology = Nat_Fascism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Diego Luis Suñer"
	picture = "Portrait_Diego_Suner.dds"
	traits = { organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Bari del Valle Sosa"
	picture = "Portrait_Bari_del_Valle_Sosa.dds"
	traits = { fast_planner }
	skill = 3
}

create_corps_commander = {
	name = "Enrique Victor Amrein"
	picture = "Portrait_Enrique_Victor_Amrein.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Agustin Cejas"
	picture = "Portrait_Agustin_Cejas.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alberto Corvalan"
	picture = "Portrait_Alberto_Corvalan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alberto Sigon"
	picture = "Portrait_Alberto_Sigon.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Aldo Fernandez"
	picture = "Portrait_Aldo_Fernandez.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Aldo Sala"
	picture = "Portrait_Aldo_Sala.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alejandro Martelletti"
	picture = "Portrait_Alejandro_Martelletti.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Alfredo Pérez Aquino"
	picture = "Portrait_Carlos_Aquino.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Carlos Nogueira"
	picture = "Portrait_Carlos_Nogueira.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Podio"
	picture = "Portrait_Carlos_Podio.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Sityar"
	picture = "Portrait_Carlos_Sityar.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Claudio Gallardo"
	picture = "Portrait_Claudio_Gallardo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Clemente Magallanes"
	picture = "Portrait_Clemente_Magallanes.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Eduardo Varela"
	picture = "Portrait_Daniel_Varela.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Claudio Ernesto Pasqualini"
	picture = "Portrait_Ernesto_Pasqualini.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Federico Sidders"
	picture = "Portrait_Federico_Sidders.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Fernando Mauricio Ros"
	picture = "Portrait_Fernando_Ros.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Gerardo Ferrara"
	picture = "Portrait_Gerardo_Ferrara.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "German Monge"
	picture = "Portrait_German_Monge.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Guillermo Pereda"
	picture = "Portrait_Guillermo_Pereda.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Guillermo Ángel Tabernero"
	picture = "Portrait_Guillermo_Tabernero.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Gustavo Booth"
	picture = "Portrait_Gustavo_Booth.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Luzuriaga"
	picture = "Portrait_Gustavo_Luzuriaga.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Planes"
	picture = "Portrait_Gustavo_Planes.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Vidal"
	picture = "Portrait_Gustavo_Vidal.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Héctor Horacio Prechi"
	picture = "Portrait_Hector_Prechi.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hugo Gargano"
	picture = "Portrait_Hugo_Gargano.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Hugo Leonard"
	picture = "Portrait_Hugo_Leonard.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Javier Abregu"
	picture = "Portrait_Javier_Abregu.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Javier Aquino"
	picture = "Portrait_Javier_Aquino.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Joaquin Estrada"
	picture = "Portrait_Joaquin_Estrada.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jose Navarro"
	picture = "Portrait_Jose_Navarro.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "José Antonio Saumell Robert"
	picture = "Portrait_Jose_Robert.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Jose Stanchina"
	picture = "Portrait_Jose_Stanchina.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "José Luis Yofre"
	picture = "Portrait_Jose_Yofre.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Juan Antonio Zamora"
	picture = "Portrait_Juan_Antonio_Zamora.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Adrián Campitelli"
	picture = "Portrait_Juan_Campitelli.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Juan Gettig"
	picture = "Portrait_Juan_Gettig.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Paleo"
	picture = "Portrait_Juan_Paleo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Juan Pulleiro"
	picture = "Portrait_Juan_Pulleiro.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Justo Treviranus"
	picture = "Portrait_Justo_Treviranus.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Luis Ricciardi"
	picture = "Portrait_Luis_Ricciardi.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Maria Pansa"
	picture = "Portrait_Maria_Pansa.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Martin Deimundo"
	picture = "Portrait_Martin_Deimundo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Miguel Lugand"
	picture = "Portrait_Miguel_Lugand.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Norberto Pastor"
	picture = "Portrait_Norberto_Pastor.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Osvaldo Guardone"
	picture = "Portrait_Osvaldo_Guardone.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Osvaldo Jose Suárez"
	picture = "Portrait_Osvaldo_Suarez.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Santiago Julio Ferreyra"
	picture = "Portrait_Santiago_Ferreyra.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Sergio Santulario"
	picture = "Portrait_Sergio_Santulario.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Tomas Moyano"
	picture = "Portrait_Tomas_Moyano.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Bernardo Noziglia"
	picture = "Portrait_Bernardo_Noziglia.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Cesar Recalde"
	picture = "Portrait_Cesar_Recalde.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Dardo Difalco"
	picture = "Portrait_Dardo_Difalco.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "David Burden"
	picture = "Portrait_David_Burden.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Bacchi"
	picture = "Portrait_Eduardo_Bacchi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Malchiodi"
	picture = "Portrait_Eduardo_Malchiodi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Sancet"
	picture = "Portrait_Eduardo_Sancet.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Francisco Medrano"
	picture = "Portrait_Francisco_Medrano.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gabriel Gonzalez"
	picture = "Portrait_Gabriel_Gonzalez.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "German Arbizu"
	picture = "Portrait_German_Arbizu.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Guillermo Cormick"
	picture = "Portrait_Guillermo_Cormick.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gustavo Iglesias"
	picture = "Portrait_Gustavo_Iglesias.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gustavo Vignale"
	picture = "Portrait_Gustavo_Vignale.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Jorge Cisneros"
	picture = "Portrait_Jorge_Cisneros.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Jose Villan"
	picture = "Portrait_Jose_Villan.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Luis Gottardo"
	picture = "Portrait_Luis_Gottardo.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Luis Mazzeo"
	picture = "Portrait_Luis_Mazzeo.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Marcelo Eduardo Hipolito Srur"
	picture = "Portrait_Marcelo_Srur.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Marcos Henson"
	picture = "Portrait_Marcos_Henson.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Maria Uriarte"
	picture = "Portrait_Maria_Uriarte.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Miguel Ángel Máscolo"
	picture = "Portrait_Miguel_Mascolo.dds"
	traits = { blockade_runner }
	skill = 3
}

create_navy_leader = {
	name = "Oscar Pichel"
	picture = "Portrait_Oscar_Pichel.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Oscar Vivas"
	picture = "Portrait_Oscar_Vivas.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Osvaldo Vernazza"
	picture = "Portrait_Osvaldo_Vernazza.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Pedro Galardi"
	picture = "Portrait_Pedro_Galardi.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Rafael Prieto"
	picture = "Portrait_Rafael_Prieto.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Rodolfo Larrosa"
	picture = "Portrait_Rodolfo_Larrosa.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Zenon Bolino"
	picture = "Portrait_Zenon_Bolino.dds"
	traits = {  }
	skill = 1
}

}