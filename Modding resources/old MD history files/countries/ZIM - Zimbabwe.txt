﻿capital = 782

oob = "ZIM_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_ndebele
set_country_flag = country_language_shona

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	commonwealth_of_nations_member
	ZIM_disgruntled_veterans
}

set_politics = {

	parties = {
		fascist = {
			popularity = 10
		}
		nationalist = {
			popularity = 50
		}
		conservative = {
			popularity = 2
		}
		market_liberal = {
			popularity = 2
		}
		social_liberal = {
			popularity = 9
		}
		social_democrat = {
			popularity = 25
		}
		communist = {
			popularity = 2
		}
	}
	
	ruling_party = nationalist
	last_election = "1996.3.16"
	election_frequency = 72
	elections_allowed = yes
}

create_country_leader = {
	name = "Robert Mugabe"
	picture = "Robert_Mugabe.dds"
	ideology = black_supremacist
}

create_country_leader = {
	name = "Robert Mugabe"
	picture = "Robert_Mugabe.dds"
	ideology = proto_fascist
}

create_country_leader = {
	name = "Morgan Tsvangirai"
	picture = "Morgan_Tsvangirai.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Welshman Ncube"
	picture = "Welshman_Ncube.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Dumiso Dabengwa"
	picture = "Dumiso_Dabengwa.dds"
	ideology = leninist
}

create_country_leader = {
	name = "Kisinoti Mukwazhe"
	picture = "Kisinoti_Mukwazhe.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Simba Makoni"
	picture = "Simba_Makoni.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Ian Smith"
	picture = "Ian_Smith.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Ian Smith"
	picture = "Ian_Smith.dds"
	ideology = fiscal_conservative
}

create_corps_commander = {
	name = "Constantine Chiwenga"
	picture = "generals/Constantine_Chiwenga.dds"
	skill = 1
}

2003.1.1 = {
	remove_ideas = commonwealth_of_nations_member
}

2016.1.1 = {
	set_politics = {
		parties = {
			fascist = { popularity = 7 }
			nationalist = { popularity = 55 }
			conservative = { popularity = 1 }
			social_liberal = { popularity = 32 }
			democratic_socialist = { popularity = 5 }
			communist = { popularity = 0 }
		}

		ruling_party = nationalist
		last_election = "2013.7.31"
		election_frequency = 72
		elections_allowed = yes
	}
}