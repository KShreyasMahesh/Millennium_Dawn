﻿capital = 543

oob = "SSD_2016"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	extensive_conscription
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 2
		}
		reactionary = {
			popularity = 94
		}
		social_liberal = {
			popularity = 2
		}
		social_democrat = {
			popularity = 2
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = reactionary
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Salva Kiir Mayardit"
	picture = "Salva_Kiir.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Dr. Lam Akol"
	picture = "Lam_Akol.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Federico Awi Vuni"
	picture = "Federico_Awi_Vuni.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Toby Maduot"
	picture = "Toby_Maduot.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Joseph Wol Modesto"
	ideology = leninist
}

create_country_leader = {
	name = "Simon Achien Majur"
	ideology = progressive_ideology
}

create_corps_commander = {
	name = "Dom Deng"
	picture = "generals/Dom_Deng"
	skill = 1
}

create_corps_commander = {
	name = "Paul Malong"
	picture = "generals/Paul_Malong"
	skill = 1
}