﻿# EU Vote mechanism
add_namespace = EU

country_event = {
	id = EU.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event EU.1" }
	title = EU.1.t
	desc = EU.1.d
	
	is_triggered_only = yes

	immediate = { }
	
	option = {
		name = EU.1.a
		log = "[GetDateText]: [This.GetName]: EU.1.a executed"
		set_global_flag = { flag = EU_majority_vote value = 1000 }
		every_country = { 
				limit = { 
					has_idea = EU_member
					}
			hidden_effect = { country_event = { id = EU.2 hours = 6 } }		
		}
		hidden_effect = { news_event = { id = EU.3 days = 2 } }
	}
}

#Here only EU members get event
country_event = {
	id = EU.2
	immediate = { log = "[GetDateText]: [Root.GetName]: event EU.2" }
	title = EU.2.t
	desc = EU.2.d
	
	is_triggered_only = yes

	immediate = { }
	
	#yes
	option = {
		name = EU.2.a
		log = "[GetDateText]: [This.GetName]: EU.2.a executed"
		ai_chance = { factor = 50 }
		set_country_flag = EU_voted_yes #for display
		EU_majority_vote_yes = yes
	}
	#no
	option = {
		name = EU.2.b
		log = "[GetDateText]: [This.GetName]: EU.2.b executed"
		ai_chance = { factor = 50 }
		set_country_flag = EU_voted_no #for display
		EU_majority_vote_no = yes
	}
	#abstain
	option = {
		name = EU.2.c
		log = "[GetDateText]: [This.GetName]: EU.2.c executed"
		ai_chance = { factor = 0 }
		set_country_flag = EU_abstained #for display
	}
}

news_event = {
	
	id = EU.3
	immediate = { log = "[GetDateText]: [Root.GetName]: event EU.3" }
	title = EU.3.t
	desc = EU.3.d
	
	major = yes
	is_triggered_only = yes
	
	immediate = { 
		Display_EU_vote_results = yes
	}
	
	option = {
		name = EU.3.a
		log = "[GetDateText]: [This.GetName]: EU.3.a executed"
		Delete_EU_vote_flags = yes
		
		if = { limit = { BUL = { has_country_flag = EU_abstained } }
		BUL_GetNameWithFlag = yes		
		}
	}
}