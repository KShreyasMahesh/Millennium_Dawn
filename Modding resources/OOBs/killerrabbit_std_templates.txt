﻿#Could you make, or have someone make, a few reasonable, good-for-most-nations templates for garrisons, infantry, militia, mechanized, armor, marines, airborne, air assault, special forces?

division_template = {
	name = "Garrison Brigade" #good at defending

	regiments = {
	
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		
		L_Engi_Bat = { x = 0 y = 0 }

	}
	support = {
	
	}
}

division_template = {
	name = "Militia Brigade" #built for maximal cheapness

	regiments = {
	
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
		Militia_Bat = { x = 0 y = 3 }
		Militia_Bat = { x = 0 y = 4 }

	}
	support = {
	
	}
}

division_template = {
	name = "Motorized Militia Brigade"	#Less cheap but has great mobility ... ok you didn't ask for it but...

	regiments = {
	
		Mot_Militia_Bat = { x = 0 y = 0 }
		Mot_Militia_Bat = { x = 0 y = 1 }
		Mot_Militia_Bat = { x = 0 y = 2 }
		Mot_Militia_Bat = { x = 0 y = 3 }

	}
	support = {
	
	}
}

division_template = {
	name = "Marine Brigade"

	regiments = {
	
		Mot_Marine_Bat = { x = 0 y = 0 }
		Mot_Marine_Bat = { x = 0 y = 1 }
		Mot_Marine_Bat = { x = 0 y = 2 }
		Mot_Marine_Bat = { x = 0 y = 3 }
		
		SP_Arty_Bat = { x = 0 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Special Operation Brigade"

	regiments = {
	
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }

	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Airborne Brigade"

	regiments = {
	
		Mot_Air_Inf_Bat = { x = 0 y = 0 }
		Mot_Air_Inf_Bat = { x = 0 y = 1 }
		Mot_Air_Inf_Bat = { x = 0 y = 2 }
		Mot_Air_Inf_Bat = { x = 0 y = 3 }

	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Air Assault Brigade"

	regiments = {
	
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		L_Air_assault_Bat = { x = 0 y = 3 }

	}
	support = {
	
	}
}

division_template = {
	name = "Light Infantry Brigade" #good for difficult terrain

	regiments = {
	
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		
		L_Engi_Bat = { x = 1 y = 0 }
		L_Recce_Bat = { x = 1 y = 1 }
		
		Arty_Bat = { x = 2 y = 0 }

	}
	support = {
	
	}
}

division_template = {
	name = "Motorized Brigade" #no tanks for simplicity

	regiments = {
	
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		Mot_Recce_Bat = { x = 0 y = 4 }
		
		Arty_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 2 y = 1 }

	}
	support = {
		L_Engi_Bat = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mechanized Brigade"	#decent sized powerful unit

	regiments = {
	
		armor_Bat = { x = 0 y = 0 }
		
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
		Mech_Recce_Bat = { x = 1 y = 3 }
		
		SP_Arty_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Armored Brigade" #very powerful and expensive, require a lot of different equipment

	regiments = {
	
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		
		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		Arm_Recce_Bat = { x = 1 y = 2 }
		
		SP_Arty_Bat = { x = 2 y = 0 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
	}
}

#### some other bde samples for reference, same as real units ####
## typical russian unit - large, heavy and stacked to the max with artillery - not typical for the ROTW

division_template = {
	name = "Guards Motor Rifle Brigade"

	regiments = {						
		Arm_Inf_Bat = { x = 0 y = 0 } 
		Arm_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Mech_Recce_Bat = { x = 0 y = 3 }
		
		armor_Bat = { x = 1 y = 0 }
		
		SP_Arty_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
		SP_Arty_Bat = { x = 2 y = 2 }
		SP_AA_Bat = { x = 2 y = 3 }
		
		H_Engi_Bat = { x = 3 y = 1 }
	}
}

division_template = {
	name = "Stryker Brigade" #US Stryker BCT - a anomality for not using tanks

	regiments = {						
		Mech_Inf_Bat = { x = 0 y = 0 } 
		Mech_Inf_Bat = { x = 0 y = 1 } 
		Mech_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }
		
		Mech_Recce_Bat = { x = 1 y = 0 }
		
		SP_Arty_Bat = { x = 1 y = 0 }
		H_Engi_Bat = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Armored Brigade" #US Armored Brigade Combat Brigade - again, more infantry than tanks

	regiments = {
		
		armor_Bat = { x = 0 y = 0 } 
		armor_Bat = { x = 0 y = 1 }
		
		Arm_Inf_Bat = { x = 1 y = 0 } 
		Arm_Inf_Bat = { x = 1 y = 1 }
		Arm_Recce_Bat = { x = 1 y = 2 }
		
		SP_Arty_Bat = { x = 2 y = 0 }
		H_Engi_Bat = { x = 2 y = 1 }
	}
}
