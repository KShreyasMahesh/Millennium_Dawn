MD4 Land Doctrine Effects Draft:

#TODO make it compact vertically and wider

legacy_doctrines:
#nonthing since it really isn't anything on its own but a generic term

- modern_blitzkrieg:

enable_tactic = tactic_blitz

- forward_defense:
#difficult to simulate
category_army = {
	org_loss_when_moving = -0.25
	dig_in_speed_factor = 0.25
}

- encourage_nco_iniative:
#The idea is that NCO's have more iniative therefore they're more likely to succeed in combat
category_front_line = {
	soft_attack = 0.05
	hard_attack = 0.05
	defense = 0.05
}


- air_land_battle:

army_bonus_air_superiority_factor = 0.25

- grand_battleplan:

planning_speed = 0.25
max_planning = 0.25
enable_tactic = tactic_planned_attack

- rigid_hierarchy:

max_organisation = 4

- praetorian_guard:

partisan_effect = -0.25 #the idea is that your pop is less likely to revolt against a foreign army if you have 
enemy_partisan_effect = 0.25 #in return enemies will revolt less

- armoured_mass_assault:

land_reinforce_rate = 0.05
category_all_armor = {
	breakthrough = 0.10
}


- deep_echelon_advance:

maximum_speed = 0.1
category_all_armor = {
	hard_attack = 0.05
	soft_attack = 0.05
}

- army_group_operational_freedom:

planning_speed = 0.15

- massed_artillery:

enable_tactic = tactic_barrage

- infiltration_assault:

enable_tactic = tactic_infantry_charge

- defence_in_depth:

enable_tactic = tactic_elastic_defense

- armoured_counterpunch:

category_all_armor = {
	defense = 0.1
	max_organisation = 5
}

- frontline_defence:

category_army = {
	max_dig_in = 5
	dig_in_speed_factor = 0.25
}

- early_tunnel_warfare:

category_army = {
	max_dig_in = 10
}

- guerilla_specialisation:

partisan_effect = 0.25