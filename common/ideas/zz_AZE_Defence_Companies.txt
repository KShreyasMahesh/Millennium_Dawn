ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		AZE_azerbaijani_defense_industry_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AZE_azerbaijani_defense_industry_infantry_weapon_company" }
		
			picture = Azerbaijani_Defense_Industry_AZE
			

			available = {
				OR = {
					original_tag = AZE
					controls_state = 713
				}
			}
			visible = {
				OR = {
					original_tag = AZE
					# controls_state = 713
					AND = {
						controls_state = 713
						has_better_than_INF_4 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.124
			}
			
			traits = {
				Cat_INF_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		AZE_azerbaijani_defense_industry_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AZE_azerbaijani_defense_industry_vehicle_company" }
		
			picture = Azerbaijani_Defense_Industry_AZE
			

			available = {
				OR = {
					original_tag = AZE
					controls_state = 713
				}
			}
			visible = {
				OR = {
					original_tag = AZE
					# controls_state = 713
					AND = {
						controls_state = 713
						has_better_than_AFV_4 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.124
			}
			
			traits = {
				Cat_AFV_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
