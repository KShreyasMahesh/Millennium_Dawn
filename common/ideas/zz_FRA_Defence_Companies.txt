ideas = {

	Helicopter_Company = {
	
		designer = yes
		
		FRA_airbus_helicopters_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_airbus_helicopters_helicopter_company" }
		
			picture = Airbus_Helicopters_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 60
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 60
					AND = {
						controls_state = 60
						has_better_than_HELI_8 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.248
			}
			
			traits = {
				Cat_HELI_8
			
			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		FRA_nexter_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_nexter_infantry_weapon_company" }
		
			picture = Nexter_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 59
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 59
					AND = {
						controls_state = 59
						has_better_than_INF_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.217
			}
			
			traits = {
				Cat_INF_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		FRA_nexter_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_nexter_vehicle_company" }
		
			picture = Nexter_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 59
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 59
					AND = {
						controls_state = 59
						has_better_than_ARMOR_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.217
			}
			
			traits = {
				Cat_ARMOR_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		FRA_dassault_aviation_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_dassault_aviation_aircraft_company" }
		
			picture = Dassault_Aviation_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 56
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 56
					AND = {
						controls_state = 56
						has_better_than_FIGHTER_8 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.248
			}
			
			traits = {
				Cat_FIGHTER_8
			
			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIGHTER_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_fighters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		FRA_naval_group_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_naval_group_ship_company" }
		
			picture = Naval_Group_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 56
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 56
					AND = {
						controls_state = 56
						has_better_than_NAVAL_EQP_8 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.248
			}
			
			traits = {
				Cat_NAVAL_EQP_8
			
			}
			ai_will_do = {
				factor = 0.8
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Submarine_Company = {
	
		designer = yes
		
		FRA_naval_group_submarine_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_naval_group_submarine_company" }
		
			picture = Naval_Group_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 56
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 56
					AND = {
						controls_state = 56
						has_better_than_SUB_8 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_SUB = 0.248
			}
			
			traits = {
				Cat_SUB_8
			
			}
			ai_will_do = {
				factor = 0.8
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
						factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
						is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_SUB_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_submarine = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		FRA_airbus_defence_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_airbus_defence_aircraft_company" }
		
			picture = Airbus_Defence_FRA
			

			available = {
				OR = {
					original_tag = FRA
					controls_state = 60
				}
			}
			visible = {
				OR = {
					original_tag = FRA
					# controls_state = 60
					AND = {
						controls_state = 60
						has_better_than_FIXED_WING_9 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.279
			}
			
			traits = {
				CAT_FIXED_WING_9
			
			}
			ai_will_do = {
				factor = 0.9 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIXED_WING_9 = yes
					factor = 0
				}
				modifier = {
					is_researching_fixed_wing = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_fighters = yes
						has_FIGHTER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_heavy_air = yes
						has_H_AIR_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_light_fighter = yes
						has_L_Fighter_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
}
