ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		SOV_military_industry_company_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_military_industry_company_vehicle_company" }
		
			picture = Military_Industry_Company_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.155
			}
			
			traits = {
				Cat_ARTILLERY_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARTILLERY_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_artillery = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		SOV_rostec_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_infantry_weapon_company" }
		
			picture = Rostec_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.248
			}
			
			traits = {
				Cat_INF_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SOV_rostec_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_vehicle_company" }
		
			picture = Rostec_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.248
			}
			
			traits = {
				Cat_ARMOR_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		SOV_rostec_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_helicopter_company" }
		
			picture = Rostec_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.248
			}
			
			traits = {
				Cat_HELI_8
			
			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		SOV_united_aircraft_corporation_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_aircraft_corporation_aircraft_company" }
		
			picture = United_Aircraft_Corporation_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.248
			}
			
			traits = {
				CAT_FIXED_WING_8
			
			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIXED_WING_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_fixed_wing = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_fighters = yes
						has_FIGHTER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_heavy_air = yes
						has_H_AIR_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_light_fighter = yes
						has_L_Fighter_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		SOV_mil_helicopters_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_mil_helicopters_helicopter_company" }
		
			picture = Mil_Helicopters_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.248
			}
			
			traits = {
				Cat_HELI_8
			
			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		SOV_irkut_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_irkut_aircraft_company" }
		
			picture = Irkut_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_FIGHTER = 0.216
			}
			
			traits = {
				Cat_FIGHTER_7
			
			}
			ai_will_do = {
				factor = 0.6 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIGHTER_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_fighters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		SOV_almaz_antey_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_almaz_antey_infantry_weapon_company" }
		
			picture = Almaz_Antey_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.248
			}
			
			traits = {
				Cat_INF_WEP_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		SOV_jsc_defense_systems_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_jsc_defense_systems_infantry_weapon_company" }
		
			picture = JSC_Defense_systems_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AA = 0.186
			}
			
			traits = {
				Cat_AA_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AA_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_aa = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		SOV_united_shipbuilding_corporation_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_ship_company" }
		
			picture = United_Shipbuilding_Corporation_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 644
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 644
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}
			
			traits = {
				Cat_NAVAL_EQP_6
			
			}
			ai_will_do = {
				factor = 0.6
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Submarine_Company = {
	
		designer = yes
		
		SOV_united_shipbuilding_corporation_submarine_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_submarine_company" }
		
			picture = United_Shipbuilding_Corporation_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 644
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 644
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_SUB = 0.186
			}
			
			traits = {
				Cat_SUB_6
			
			}
			ai_will_do = {
				factor = 0.6
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
						factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
						is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_SUB_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_submarine = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SOV_kurganmashzavod_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_kurganmashzavod_vehicle_company" }
		
			picture = Kurganmashzavod_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.217
			}
			
			traits = {
				Cat_AFV_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		SOV_russian_helicopters_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_russian_helicopters_helicopter_company" }
		
			picture = Russian_Helicopters_SOV
			

			available = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			visible = {
				OR = {
					tag = SOV
					controls_state = 652
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.186
			}
			
			traits = {
				Cat_HELI_6
			
			}
			ai_will_do = {
				factor = 0.6 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
