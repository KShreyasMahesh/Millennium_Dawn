ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		HOL_dutch_defense_vehicle_systems_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_dutch_defense_vehicle_systems_vehicle_company" }
		
			picture = Dutch_Defense_Vehicle_Systems_HOL
			

			available = {
				OR = {
					original_tag = HOL
					controls_state = 46
				}
			}
			visible = {
				OR = {
					original_tag = HOL
					# controls_state = 46
					AND = {
						controls_state = 46
						has_better_than_AFV_6 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.186
			}
			
			traits = {
				Cat_AFV_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		HOL_damen_shipyards_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_damen_shipyards_ship_company" }
		
			picture = Damen_Shipyards_HOL
			

			available = {
				OR = {
					original_tag = HOL
					controls_state = 46
				}
			}
			visible = {
				OR = {
					original_tag = HOL
					# controls_state = 46
					AND = {
						controls_state = 46
						has_better_than_NAVAL_EQP_6 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}
			
			traits = {
				Cat_NAVAL_EQP_6
			
			}
			ai_will_do = {
				factor = 0.6
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
}
