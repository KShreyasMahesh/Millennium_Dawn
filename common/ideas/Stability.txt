ideas = {
	
	hidden_ideas = {
		
		unstable_neigbor_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_1" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.03
			}
		}
		unstable_neigbor_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_2" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.06
			}
		}
		unstable_neigbor_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_3" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.09
			}
		}
		unstable_neigbor_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_4" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.12
			}
		}
		unstable_neigbor_5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_5" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.15
			}
		}
		unstable_neigbor_6 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_6" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.18
			}
		}
		unstable_neigbor_7 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_7" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.21
			}
		}
		unstable_neigbor_8 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_8" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.24
			}
		}
		unstable_neigbor_9 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_9" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.27
			}
		}
		unstable_neigbor_10 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea unstable_neigbor_10" }
		

			allowed_civil_war = {
				always = yes
			}

			
			
			modifier = {
				stability_factor = -0.3
			}
		}
		
	}
}