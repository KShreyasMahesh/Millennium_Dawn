ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		ISR_israel_weapon_industries_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_weapon_industries_infantry_weapon_company" }
		
			picture = Israel_Weapon_Industries_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_INF_WEP_8 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.248
			}
			
			traits = {
				Cat_INF_WEP_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		ISR_israel_military_industries_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_military_industries_infantry_weapon_company" }
		
			picture = Israel_Military_Industries_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_INF_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.217
			}
			
			traits = {
				Cat_INF_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		ISR_soltam_systems_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_soltam_systems_vehicle_company" }
		
			picture = Soltam_Systems_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_ARTILLERY_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.155
			}
			
			traits = {
				Cat_ARTILLERY_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARTILLERY_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_artillery = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		ISR_plasan_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_plasan_vehicle_company" }
		
			picture = Plasan_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_AFV_6 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.186
			}
			
			traits = {
				Cat_AFV_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		ISR_israel_aerospace_industries_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_aerospace_industries_aircraft_company" }
		
			picture = Israel_Aerospace_Industries_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_FIXED_WING_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.217
			}
			
			traits = {
				CAT_FIXED_WING_7
			
			}
			ai_will_do = {
				factor = 0.7 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIXED_WING_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_fixed_wing = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_fighters = yes
						has_FIGHTER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_heavy_air = yes
						has_H_AIR_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_light_fighter = yes
						has_L_Fighter_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		ISR_rafael_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_rafael_infantry_weapon_company" }
		
			picture = Rafael_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_AA_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AA = 0.217
			}
			
			traits = {
				Cat_AA_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AA_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_aa = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		ISR_mantak_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_mantak_vehicle_company" }
		
			picture = MANTAK_ISR
			

			available = {
				OR = {
					original_tag = ISR
					controls_state = 207
				}
			}
			visible = {
				OR = {
					original_tag = ISR
					# controls_state = 207
					AND = {
						controls_state = 207
						has_better_than_TANKS_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_TANKS = 0.155
			}
			
			traits = {
				CAT_TANKS_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_TANKS_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_tanks = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
