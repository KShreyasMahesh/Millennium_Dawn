﻿2000.1.1 = {

	create_field_marshal = {
		name = "Zakir Hasanov"
		picture = "Portrait_Zakir_Hasanov.dds"
		traits = { old_guard organisational_leader }
		id = 301
		skill = 3
		attack_skill = 2
		defense_skill = 4
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Najmeddin Sadikov"
		picture = "Portrait_Najmeddin_Sadikov.dds"
		traits = { panzer_leader }
		id = 302
		skill = 3
		attack_skill = 3
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Elchin Guliyev"
		picture = "Portrait_Elchin_Gulyev.dds"
		traits = { ranger }
		id = 303
		skill = 3
		attack_skill = 2
		defense_skill = 4
		planning_skill = 1
		logistics_skill = 3
	}

	create_navy_leader = {
		name = "Shahin Sultanov"
		picture = "Portrait_Shahin_Sultanov.dds"
		traits = { blockade_runner }
		id = 304
	}

}