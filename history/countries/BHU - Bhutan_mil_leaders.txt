﻿2000.1.1 = {

	create_corps_commander = {
		name = "Batoo Tshering"
		picture = "Portrait_Batoo_Tshering.dds"
		traits = { organisational_leader offensive_doctrine }
		id = 481
		skill = 3
		attack_skill = 3
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
	}
	
}