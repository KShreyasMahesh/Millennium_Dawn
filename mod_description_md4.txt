﻿[h1]2017:[/h1]
The world is changing. Globalization, terrorism, climate change. The list goes on and on. As countries vie for supremacy in this new millennium, can you lead your nation to prosperity through the decades ahead? Or will you stumble through the trials of the 21st century?

[b][i]Welcome to Modern Day 4.[/i][/b]

[img]https://i.imgur.com/ZkWA9Yg.png[/img]
[img]https://i.imgur.com/RULr4Y8.png[/img]

Step into the shoes of any leader of the modern world, and experience the challenges and complexities facing the globe today. 

Completely reimagined from vanilla Hearts of Iron, Modern Day 4 seeks to turn the base game into a slow-paced geopolitical game of conflict and intrigue. Realism is key. War between the major powers will only end in destruction. Use your new abilities added in this mod to spread your influence across the planet, or shrink into isolation.

With this early release of Modern Day 4, you can expect:
[list][*]New Map (new provinces, real borders, states, resources, industry, and population)
[*]New Countries (all with portraits, correct statistics, and military leaders)
[*]New Economic System (development, debt, corruption, and budget-management)
[*]New Tech Tree (extending from 1965-2035)
[*]New Equipment and Unit types (all made from scratch to make combat a complete new experience)
[*]All countries have highly detailed and accurate armies, navy, aircraft, and equipment based on real world statistics collected by the IISS’s [i]The Military Balance 2016,[/i] SIPRI’s [i]Trends in International Arms Transfers 2016,[/i] and FlightGlobal’s [i]World Air Forces 2015.[/i]
[*]Detailed Civil Wars (if it exists, it's here)
[*]Modern 3D models
[*]All new modern sound effects[/list]

We're proud to be the only modern day Hearts of Iron mod that can tick all of the boxes above. This is a very solid base on which to build great, dynamic, and exciting gameplay.

So, we have built a solid base, but the mod is—like most others—NOT finished. In this early release, we aim for a fun gameplay experience for the first few years, especially in the Middle East. When you are finished, please reload and try a different country.

[img]https://i.imgur.com/8SISgpC.png[/img]

[b]Current Version:[/b] Alpha 0.2
[list][*]Internal Factions system.
[i]-    Fight or support the Oligarchs, Fossil Fuel Industry, Wall Street, and more![/i]
[*]Expanded Research Slots
[i]-    Now research is much slower and much more realistic.[/i]
[*]NATO mechanic.
[i]-    A new way for European nations to band together and defend themselves.[/i]
[*]Elections and approval rating system
[i]-    Vote for your future.[/i]
[/list]
[i][b]And much more![/b][/i]

[url=https://www.reddit.com/r/hoi4/comments/82q9xv/modern_day_4_v002_release_announcement/] Read the full changelog here. [/url]

[b]What can we expect in Future Releases?[/b]

Future updates will add more content across the board. Decisions, dynamic events, politics, focuses, more conflicts, and all that exciting geopolitical action we know you love and crave. Other planned features include a climate change system and natural disasters.

[h1][b]Disclaimer![/b][/h1]
If you are having issues running Hearts of Iron update 1.5 "Cornflakes," you may experience poor performance with this mod! Due to the size and scope of this mod, Modern Day 4 has higher system specs than vanilla.

Do not use other mods or submods with this mod unless the author of the submod has specifically stated that their submod works with MD4. Any crash/bug reports or balance issues made while running other active mods will be ignored. Please write to us about errors, but specify as many details as possible—otherwise it is not helpful. This mod does not require DLC to play but the MD4 team highly recommends having all DLC to get the full experience.

[img]https://i.imgur.com/a2jZFvb.png[/img]

Special thanks to all the employees at Paradox Interactive, whom without their game none of this would be possible, Also thanks to the authors of the following mods, whose elements, with permission, have been included in this mod.
[list][*][i]Millennium Dawn: Alternate Style submod[/i] by Grymic.
[*][i]Millennium Dawn: Retexture Mod[/i] by CelsiuZ.
[*][i]Modern Day 4 Generals[/i] by TimlZ.[/list]
[img]https://i.imgur.com/x8rsE1t.png[/img]

Do you want to join the Modern Day 4 development team? We’re always looking for new talent!

To be considered for application, you must either:
[list][*]Be a good scripter.
[*]Be proficient with photoshop or 3D modelling.
[*]GitHub/GitLab experience a plus.
[*]Or be willing to show dedication doing tedious easy work until you have built up the skills necessary to do your own designed contributions.[/list]

Full time members are those that are active with valuable contributions. Full time members run and decide how the mod should be based primarily on a discussion and consensus model. Thus, there is no limit to what you can help decide and do if you are a full time member putting in effort. 

Submods are allowed. No need to ask. However, taking stuff from the mod without permission, isn't.

[img]https://i.imgur.com/WdKYhJo.png[/img]

[h1][b]Join our Discord Server and Follow Us on Social Media![/b][/h1]
[url=https://discord.gg/WEXzt47]Discord[/url] 
[url=https://twitter.com/ModernDay4Mod]Twitter[/url]
[url=https://steamcommunity.com/linkfilter/?url=https://forum.paradoxplaza.com/forum/index.php?threads/modern-day-4-hoi-mod.913771/]Paradox Forums[/url]

[b][i]Happy playing![/i][/b] 